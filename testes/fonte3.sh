# !/bin/bash

if ((1.0))
	then 10+20
	else 30+40
fi

if ((1.0)); then :; fi

if ((1.0))
	then :
fi

if ((1.0))
	then :
	elif ((2.0))
	then :
	else :
fi

until ((i > 10))
do
	echo i
done

case variavel in
	1)
		echo 1
		;;
	2)
		echo 2
		;;
	3)
		echo 3
		;;
esac

case variavel in
	"string1")
		echo 1
		;;
	"string2")
		echo 2
		;;
	"string3")
		echo 3
		;;
esac
