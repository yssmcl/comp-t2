#!/bin/bash

funcaowhile() {
	i=0
	while (( i < 10 ))
	do
		echo "$i"
		((i++))
	done
}

funcaountil() {
	i=0
	until (( i >= 10 )) # ?
	do
		echo "$i"
		((i++))
	done
}

funcaoif() {
	a=1 b=2
	c=3
	local a

	if (( (a == 1 || b == 2) && (c == 4) ))
	then
		echo "true"
	else
		echo "false"
	fi 

	if (( a == 2 ))
	then
		echo "a=2"
	elif (( a == 1 ))
	then
		echo "a=1"
	else
		echo "a=?"
	fi
}

funcaocase() {
	echo -n "Vou buscar os dados do sistema. Posso continuar? [S/n] "
	read RESPOSTA
	case $RESPOSTA in 
		s|S|"")
			date
			;;
		n|N)
			echo "saindo"
			;;
		*)
			echo "entre com 's' ou 'n'"
			;;
	esac
}

funcaoop() {
	until (( op == 4 )); do
		echo 
		printf "1. funcaoif\n"
		printf "2. funcaofor\n"
		printf "3. funcaocase\n"
		printf "4. Sair\n\n"

		printf "Digite uma opção: "
		read op
		echo

		case "$op" in
			1) funcaoif    ;;
			2) funcaofor   ;;
			3) funcaocase  ;;
			4) echo "Sair" ;;

			*) echo "Digite uma opção de 1 a 4" ;;
		esac
	done
}

funcaofor() {
	lista_int="10 20 30"
	lista_str="qwe asd zxc"

	for i in $lista_int
	do
	    echo $((i + 12))
	done

	for i in $lista_str
	do
	    echo "$i"
	done

	for ((i = 0; i < 10; i++))
	do
		echo "$i"
	done
}

funcaosoma() { # chamada: funcaosoma "$@"
	if [[ ! "$2" || $# -gt 2 ]]
	then
		echo "Uso: $0 <num1> <num2> "
	else
		soma=$(($1 + $2))
		echo "$1 + $2 = $soma"
	fi
}

funcaoarray() {
	array=(1 true "asd" 2.32)
	echo "${array[1]}"
	echo "${array[@]}"
	echo "${array[*]}"
	echo {a..z}
}

funcaoteste() { echo 1; }; funcaotest2() { echo 2; }

funcaoteste
funcaotest2
