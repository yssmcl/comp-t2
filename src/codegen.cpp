#include "no.hpp"
#include "codegen.hpp"
#include "parser.hpp"

using namespace std;
static LLVMContext TheContext;
static IRBuilder<> Builder(TheContext);

// Compila a AST em um módulo
void CodeGenContext::generateCode(Bloco& root) {
	cout << "\n--- Geração de código ---\n";
	
	vector<Type*> argTypes;
	FunctionType *ftype = FunctionType::get(Type::getVoidTy(getGlobalContext()),
			makeArrayRef(argTypes), false);
	mainFunction = Function::Create(ftype, GlobalValue::InternalLinkage, "main", module);
	BasicBlock *bblock = BasicBlock::Create(getGlobalContext(), "entry", mainFunction, 0);
	
	pushBlock(bblock);
	root.codeGen(*this);
	ReturnInst::Create(getGlobalContext(), bblock);
	popBlock();
	
	// Imprime o código assembly gerado
	cout << "Código gerado.\n";
	cout << "\n--- Assembly ---\n";
	PassManager<Module> pm;
	pm.addPass(PrintModulePass(outs()));
	pm.run(*module);
}

// Executa a função main
GenericValue CodeGenContext::runCode() {
	cout << "\nExecutando código...\n";
	ExecutionEngine *ee = EngineBuilder(unique_ptr<Module>(module)).create();
	ee->finalizeObject();
	vector<GenericValue> noargs;
	GenericValue v = ee->runFunction(mainFunction, noargs); // roda o código assembly
	cout << "Código executado.\n";
	return v;
}

static Type* typeOf(int type) {
	if (type == 273) {
		return Type::getInt64Ty(getGlobalContext());
	} else if (type == 274) {
		return Type::getDoubleTy(getGlobalContext());
	}
	return Type::getVoidTy(getGlobalContext());
}

/* codeGens */

Value* NumeroInteiro::codeGen(CodeGenContext& context) {
	cout << "Criando int: " << value << endl;
	return ConstantInt::get(Type::getInt64Ty(getGlobalContext()), value, true);
}

Value* NumeroReal::codeGen(CodeGenContext& context) {
	cout << "Criando double: " << value << endl;
	return ConstantFP::get(Type::getDoubleTy(getGlobalContext()), value);
}

Value* Identificador::codeGen(CodeGenContext& context) {
	cout << "Criando identificador: " << name << endl;
	if (context.tab_simbolos().find(name) == context.tab_simbolos().end()) {
		cerr << "\nVariável '" << name << "' não declarada\n\n";
		exit(1);
		return NULL;
	}
	return new LoadInst(context.tab_simbolos()[name], "", false, context.currentBlock());
}

Value* ChamadaFuncao::codeGen(CodeGenContext& context) {
	Function *function = context.module->getFunction(id.name.c_str());
	if (function == NULL) {
		cerr << "\nFunção '" << id.name << "' não declarada\n\n";
		exit(1);
	}
	vector<Value*> args;
	vector<Expressao*>::const_iterator it;
	for (it = lista_argumentos.begin(); it != lista_argumentos.end(); it++) {
		args.push_back((**it).codeGen(context));
	}
	CallInst *call = CallInst::Create(function, makeArrayRef(args), "", context.currentBlock());
	cout << "Criando method call: " << id.name << endl;
	return call;
}

Value* OperacaoAritmetica::codeGen(CodeGenContext& context) {
	cout << "Criando operação binária: " << op << endl;
	Instruction::BinaryOps instr;
	switch (op) { 
		case AD:   instr = Instruction::Add; goto math;
		case SUB:  instr = Instruction::Sub; goto math;
		case MULT: instr = Instruction::Mul; goto math;
		case DIV:  instr = Instruction::SDiv; goto math;
		case MOD:  instr = Instruction::SRem; goto math;
		case AND:  instr = Instruction::And; goto math;
		case OR:   instr = Instruction::Or; goto math;
	}

	return NULL;
math:
	return BinaryOperator::Create(instr, lhs.codeGen(context), rhs.codeGen(context), "",
			context.currentBlock());
}

Value* OperacaoCondicional::codeGen(CodeGenContext& context) {
	cout << "Criando operação condicional " << op << endl;
	CmpInst::Predicate instr;
	Instruction::OtherOps instr2;
	int c = 0;

	switch (op) { 
		case LT: instr = CmpInst::ICMP_SLT; c++; break;
		case GT: instr = CmpInst::ICMP_SGT; c++; break;
		case LE: instr = CmpInst::ICMP_SLE; c++; break;
		case GE: instr = CmpInst::ICMP_SGE; c++; break;
		case EQ: instr = CmpInst::ICMP_EQ; c++; break;
		case NE: instr = CmpInst::ICMP_NE; c++; break;
	}

	if (c) return CmpInst::Create(instr2, instr, lhs.codeGen(context), 
		rhs.codeGen(context), "", context.currentBlock());

	return NULL;
}

Value* Bloco::codeGen(CodeGenContext& context) {
	vector<Instrucao*>::const_iterator it;
	Value *last = NULL;
	for (it = lista_instrucoes.begin(); it != lista_instrucoes.end(); it++) {
		cout << "Gerando código para '" << typeid(**it).name() << "'...\n";
		last = (**it).codeGen(context);
	}
	cout << "Criando bloco...\n";
	return last;
}

Value* InstrucaoExpressao::codeGen(CodeGenContext& context) {
	cout << "Gerando código para '" << typeid(expressao).name() << "'" << endl;
	return expressao.codeGen(context);
}

Value* DeclaracaoVariavel::codeGen(CodeGenContext& context) {
	cout << "Criando definição/declaração de variável: " << id.name << endl;
	AllocaInst *alloc = new AllocaInst(typeOf(type), id.name.c_str(), context.currentBlock());
	context.tab_simbolos()[id.name] = alloc;
	if (assignmentExpr) {
		Atribuicao assn(id, *assignmentExpr);
		assn.codeGen(context);
	}
	return alloc;
}

Value* Atribuicao::codeGen(CodeGenContext& context) {
	cout << "Criando atribuição para '" << lhs.name << "'" << endl;
	if (context.tab_simbolos().find(lhs.name) == context.tab_simbolos().end()) {
		cerr << "Variável '" << lhs.name << "' não declarada" << endl;
		return NULL;
	}
	return new StoreInst(rhs.codeGen(context), context.tab_simbolos()[lhs.name], false, context.currentBlock());
}

Value* DefinicaoFuncao::codeGen(CodeGenContext& ctx) {
	vector<Type*> argTypes;
	vector<DeclaracaoVariavel*>::const_iterator it;

	for (it = parametros.begin(); it != parametros.end(); it++) {
		argTypes.push_back(typeOf((**it).type));
	}

	// Seta o tipo de retorno da função como void
	FunctionType* ftype = FunctionType::get(Type::getVoidTy(getGlobalContext()), makeArrayRef(argTypes), false);

	// Cria a função
	Function* function = Function::Create(ftype, GlobalValue::InternalLinkage, id.name.c_str(), ctx.module);
	BasicBlock* bblock = BasicBlock::Create(getGlobalContext(), "entry", function, 0);

	ctx.pushBlock(bblock);

	Function::arg_iterator argsValues = function->arg_begin();

	for (it = parametros.begin(); it != parametros.end(); it++) {
		(**it).codeGen(ctx);

		Value* argumentValue = &*argsValues++;
		argumentValue->setName((*it)->id.name.c_str());
		StoreInst *inst = new StoreInst(argumentValue, ctx.tab_simbolos()[(*it)->id.name], false, bblock);
	}

	bloco.codeGen(ctx);
	ReturnInst::Create(getGlobalContext(), ctx.getCurrentReturnValue(), bblock);

	ctx.popBlock();
	cout << "Criando função '" << id.name << "'...\n";
	return function;

	return nullptr;
}

// Value* InstrucaoIf::codeGen(CodeGenContext& ctx) {
//     Value *CondV = cond.codeGen(ctx);
//     if (!CondV) return nullptr;

//     // Create blocks for the then and else cases.  Insert the 'then' block at the end of the function.
//     BasicBlock *EntryBB = BasicBlock::Create(getGlobalContext(), "entry");
//     BasicBlock *ThenBB = BasicBlock::Create(getGlobalContext(), "then");
//     BasicBlock *ElseBB = BasicBlock::Create(getGlobalContext(), "else");
//     BasicBlock *MergeBB = BasicBlock::Create(getGlobalContext(), "ifcont");

//     IRBuilder<> builder(EntryBB);

//     Function* TheFunction = builder.GetInsertBlock()->getParent();

//     // CondV = builder.CreateFCmpONE(CondV, ConstantFP::get(getGlobalContext(), APFloat(0.0)), "ifcond");
//     builder.CreateCondBr(CondV, ThenBB, ElseBB);
//     builder.SetInsertPoint(ThenBB);

//     Value *ThenV = bloco_then.codeGen(ctx);
//     if (!ThenV) return nullptr;
//     builder.SetInsertPoint(ElseBB);
//     builder.CreateBr(MergeBB);
//     ThenBB = builder.GetInsertBlock();
//     TheFunction->getBasicBlockList().push_back(ElseBB);

//     Value *ElseV = bloco_else.codeGen(ctx);
//     if (!ElseV) return nullptr;
//     builder.SetInsertPoint(MergeBB);
//     builder.CreateBr(MergeBB);
//     ElseBB = builder.GetInsertBlock();
//     TheFunction->getBasicBlockList().push_back(MergeBB);

//     PHINode *PN = builder.CreatePHI(Type::getDoubleTy(getGlobalContext()), 2, "iftmp");
//     PN->addIncoming(ThenV, ThenBB);
//     PN->addIncoming(ElseV, ElseBB);
//     return PN;
// }

Value* InstrucaoIf::codeGen(CodeGenContext& ctx) {
	Value *CondV = cond.codeGen(ctx);
	BasicBlock *bb = BasicBlock::Create(getGlobalContext(), "entry");

	BasicBlock* then_bb = BasicBlock::Create(getGlobalContext(), "then");
	BasicBlock* else_bb = BasicBlock::Create(getGlobalContext(), "else");
	BasicBlock* exit_bb = BasicBlock::Create(getGlobalContext(), "exit");

	IRBuilder<> builder(bb);

	// Value* condition = builder.CreateFCmpONE(CondV, ConstantFP::get(getGlobalContext(), APFloat(0.0)), "ifcond");

	// bb->getInstList().push_back(BranchInst::Create(then_bb, else_bb, condition));

	builder.CreateCondBr(CondV, then_bb, else_bb);
	builder.SetInsertPoint(then_bb);
	bloco_then.codeGen(ctx);
	then_bb->getInstList().push_back( BranchInst::Create( exit_bb ) );
	else_bb->getInstList().push_back( BranchInst::Create( exit_bb ) );

	// this->fill_in( then_bb );
	// this->fill_in( else_bb );

	return exit_bb;
}

Value* InstrucaoWhile::codeGen(CodeGenContext& ctx) {
	BasicBlock *LoopBB = BasicBlock::Create(getGlobalContext(), "loop");

	IRBuilder<> builder(LoopBB);

	Function *TheFunction = builder.GetInsertBlock()->getParent();
	BasicBlock *PreheaderBB = builder.GetInsertBlock();

	builder.CreateBr(LoopBB);
	builder.SetInsertPoint(LoopBB);

	if (!bloco.codeGen(ctx)) return nullptr;
	Value *EndCond = cond.codeGen(ctx);
	if (!EndCond) return nullptr;

	// EndCond = builder.CreateFCmpONE(EndCond, ConstantFP::get(getGlobalContext(), APFloat(0.0)), "loopcond");
	BasicBlock *LoopEndBB = builder.GetInsertBlock();
	BasicBlock *AfterBB = BasicBlock::Create(getGlobalContext(), "afterloop", TheFunction);

	builder.CreateCondBr(EndCond, LoopBB, AfterBB);

	builder.SetInsertPoint(AfterBB);

	return Constant::getNullValue(Type::getDoubleTy(getGlobalContext()));
}
