#include <iostream>
#include "codegen.hpp"
#include "no.hpp"

extern int yyparse();
extern Bloco* blocoPrincipal;

int main(void) {
	yyparse();

	InitializeNativeTarget();
	InitializeNativeTargetAsmPrinter();
	InitializeNativeTargetAsmParser();

	CodeGenContext context;
	context.generateCode(*blocoPrincipal);
	// context.runCode();
}
