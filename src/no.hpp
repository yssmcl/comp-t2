#include <iostream>
#include <vector>
#include <llvm/IR/Value.h>

using namespace std;

class CodeGenContext;
class Instrucao;
class Expressao;
class DeclaracaoVariavel;

class No {
public:
    virtual ~No() {}
    virtual llvm::Value* codeGen(CodeGenContext& context) { return NULL; }
};

class Expressao : public No {
};

class Instrucao : public No {
};

class Bloco : public Instrucao {
public:
	vector<Instrucao*> lista_instrucoes;

	Bloco() {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class OperacaoAritmetica : public Expressao {
public:
	Expressao& lhs;
	Expressao& rhs;
	int op;

    OperacaoAritmetica(Expressao& lhs, int op, Expressao& rhs) :
        lhs(lhs), rhs(rhs), op(op) {}

    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class OperacaoCondicional : public Expressao {
public:
	Expressao& lhs;
	Expressao& rhs;
	int op;

	OperacaoCondicional(Expressao& lhs, int op, Expressao& rhs) :
		lhs(lhs), rhs(rhs), op(op) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class OperadorUnario : public Expressao {
public:
	int op;
	Expressao& lhs;

	OperadorUnario(int op, Expressao& lhs) :
		op(op), lhs(lhs) {}

	// virtual llvm::Value* codeGen(CodeGenContext& context);
};

class InstrucaoExpressao : public Instrucao {
public:
    Expressao& expressao;

    InstrucaoExpressao(Expressao& expressao) :
		expressao(expressao) {}

    virtual llvm::Value* codeGen(CodeGenContext& context);
};

class Identificador : public Expressao {
public:
	string name;

	Identificador(const string& name) :
		name(name) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NumeroInteiro : public Expressao {
public:
	long long value;

	NumeroInteiro(long long value) :
		value(value) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NumeroReal : public Expressao {
public:
	double value;

	NumeroReal(double value) :
		value(value) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class DefinicaoFuncao : public Instrucao {
public:
	const Identificador& id;
	vector<DeclaracaoVariavel*> parametros;
	Bloco& bloco;

	DefinicaoFuncao(const Identificador& id, const vector<DeclaracaoVariavel*>& parametros, Bloco& bloco) :
		id(id), parametros(parametros), bloco(bloco) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class ChamadaFuncao : public Instrucao {
public: 
	const Identificador& id;
	vector<Expressao*> lista_argumentos;

	ChamadaFuncao(const Identificador& id, vector<Expressao*>& lista_argumentos) : 
		id(id), lista_argumentos(lista_argumentos) {}

	ChamadaFuncao(const Identificador& id) :
		id(id) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class Atribuicao : public Instrucao {
public:
	Identificador& lhs;
	Expressao& rhs;

	Atribuicao(Identificador& lhs, Expressao& rhs) :
		lhs(lhs), rhs(rhs) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class DeclaracaoVariavel : public Instrucao {
public:
	int type; // 'int' ou 'double'
	Identificador& id;
	Expressao *assignmentExpr;

	DeclaracaoVariavel(int type, Identificador& id) :
		type(type), id(id) { assignmentExpr = NULL; }

	DeclaracaoVariavel(int type, Identificador& id, Expressao *assignmentExpr) :
		type(type), id(id), assignmentExpr(assignmentExpr) {}

	DeclaracaoVariavel(Identificador& id, Expressao *assignmentExpr) :
		id(id), assignmentExpr(assignmentExpr) { type = 0; }

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class InstrucaoWhile : public Bloco {
public:
	Expressao& cond;
	Bloco& bloco;

	InstrucaoWhile(Expressao& cond, Bloco& bloco) :
		cond(cond), bloco(bloco) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class InstrucaoUntil : public Bloco {
public:
    Expressao& expressao;
	Bloco& bloco;

	InstrucaoUntil(Expressao& expressao, Bloco& bloco) :
		expressao(expressao), bloco(bloco) {}
};

class InstrucaoFor : public Bloco {
public:
	Instrucao* instrucao1;
	Expressao* expressao1;
	Expressao* expressao2;
	Instrucao* instrucao2;
	Bloco& bloco;

	InstrucaoFor(Expressao* expressao1, Expressao* expressao2, Bloco& bloco) :
		expressao1(expressao1), expressao2(expressao2), bloco(bloco) {
			instrucao1 = NULL;
			instrucao2 = NULL;
		}

	InstrucaoFor(Instrucao* instrucao1, Expressao* expressao1, Instrucao* instrucao2, Bloco& bloco) :
		instrucao1(instrucao1), expressao1(expressao1), instrucao2(instrucao2), bloco(bloco) {
			expressao2 = NULL;
		}
};

class InstrucaoIf : public Bloco {
public:
    Expressao& cond;
	Bloco& bloco_then;
	Bloco& bloco_else;

	// if-then-else
	InstrucaoIf(Expressao& cond, Bloco& bloco_then, Bloco& bloco_else) :
		cond(cond), bloco_then(bloco_then), bloco_else(bloco_else) {}

	// if-then
	InstrucaoIf(Expressao& cond, Bloco& bloco_then) :
		cond(cond), bloco_then(bloco_then), bloco_else(bloco_then) {}

	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class InstrucaoCase : public Bloco {
public:
    Expressao& expressao;

	InstrucaoCase(Expressao& expressao) :
		expressao(expressao) {}
};
