#include <stack>
#include <typeinfo>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/PassManager.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/MCJIT.h>
#include <llvm/ExecutionEngine/GenericValue.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/InstrTypes.h>

using namespace llvm;

class Bloco;

class CodeGenBlock {
public:
    BasicBlock *block;
    Value *returnValue;
    std::map<std::string, Value*> tab_simbolos;
};

class CodeGenContext {
    std::stack<CodeGenBlock *> blocks;
    Function *mainFunction;

public:
	Module *module;

	void generateCode(Bloco& root);
	GenericValue runCode();

    CodeGenContext() {
		module = new Module("main", getGlobalContext());
	}

    std::map<std::string, Value*>& tab_simbolos() {
		return blocks.top()->tab_simbolos;
	}

    BasicBlock *currentBlock() {
		return blocks.top()->block;
	}

    void pushBlock(BasicBlock *block) {
		blocks.push(new CodeGenBlock());
		blocks.top()->returnValue = NULL;
		blocks.top()->block = block;
	}

    void popBlock() {
		CodeGenBlock *top = blocks.top();
		blocks.pop();
		delete top;
	}

    void setCurrentReturnValue(Value *value) {
		blocks.top()->returnValue = value;
	}

    Value* getCurrentReturnValue() {
		return blocks.top()->returnValue;
	}
};
