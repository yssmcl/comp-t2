%{
#include <string>
#include "no.hpp"
#include "parser.hpp"

#define SAVE_TOKEN  yylval.string = new std::string(yytext, yyleng)
#define TOKEN(t)    (yylval.token = t)

void yyerror(const char*); 
void comment();
void count();
%}

%option noyywrap yylineno

letra  [a-zA-Z]
digito [0-9]

%%
	/* Palavras reservadas */
"case"                     { count(); return TOKEN(CASE);             }
"do"                       { count(); return TOKEN(DO);               }
"done"                     { count(); return TOKEN(DONE);             }
"elif"                     { count(); return TOKEN(ELIF);             }
"else"                     { count(); return TOKEN(ELSE);             }
"esac"                     { count(); return TOKEN(ESAC);             }
"fi"                       { count(); return TOKEN(FI);               }
"for"                      { count(); return TOKEN(FOR);              }
"if"                       { count(); return TOKEN(IF);               }
"in"                       { count(); return TOKEN(IN);               }
"then"                     { count(); return TOKEN(THEN);             }
"until"                    { count(); return TOKEN(UNTIL);            }
"while"                    { count(); return TOKEN(WHILE);            }
"function"                 { count(); return TOKEN(FUNCTION);         }
":"                        { count(); return TOKEN(DOIS_PONTOS);      }
"int"                      { count(); return TOKEN(INT);              }
"double"                   { count(); return TOKEN(DOUBLE);           }

	/* Strings */
{letra}({letra}|{digito})* { count(); SAVE_TOKEN; return ID;          }
{digito}+                  { count(); SAVE_TOKEN; return NUM_INTEIRO; }
{digito}+"."{digito}*      { count(); SAVE_TOKEN; return NUM_REAL;    }
\"(\\.|[^\\"])*\"          { count(); SAVE_TOKEN; return STRING;      }   

	/* Operadores aritméticos */
"+"                        { count(); return TOKEN(AD);               }
"-"                        { count(); return TOKEN(SUB);              }
"/"                        { count(); return TOKEN(DIV);              }
"*"                        { count(); return TOKEN(MULT);             }
"%"                        { count(); return TOKEN(MOD);              }

	/* Operadores condicionais */
"<"                        { count(); return TOKEN(LT);               }
">"                        { count(); return TOKEN(GT);               }
"<="                       { count(); return TOKEN(LE);               }
">="                       { count(); return TOKEN(GE);               }
"=="                       { count(); return TOKEN(EQ);               }
"!="                       { count(); return TOKEN(NE);               }

	/* Operadores lógicos */
"&&"                       { count(); return TOKEN(AND);              }
"||"                       { count(); return TOKEN(OR);               }

	/* Delimitadores */
"("                        { count(); return TOKEN(ABRE_PAR);         }
")"                        { count(); return TOKEN(FECHA_PAR);        }
"{"                        { count(); return TOKEN(ABRE_CH);          }
"}"                        { count(); return TOKEN(FECHA_CH);         }
"["                        { count(); return TOKEN(ABRE_COL);         }
"]"                        { count(); return TOKEN(FECHA_COL);        }

	/* Outros */
"#".*                      { comment();                               }
"."                        { count(); return TOKEN(PONTO);            }
","                        { count(); return TOKEN(VIRGULA);          }
"="                        { count(); return TOKEN(IGUAL);            }
"$"                        { count(); return TOKEN(CIFRAO);           }
";"                        { count(); return TOKEN(PEV);              }
"\n"                       { count(); return TOKEN(EOL);              }
[ \t\v\f\r]                { count();                                 }

.                          { count(); yyerror("Caractere inválido\n");
							return NIL; }
%%

void comment() {
	char c;
	while ((c = yyinput()) != '\n' && c != 0) putchar(c); // yyinput() pra C++, input() pra C
}

int coluna = 0;
int linha = 1;
void count() {
	int i;
	for (i = 0; yytext[i] != '\0'; i++) {
		if (yytext[i] == '\n') {
			linha++;
			coluna = 0;
		}
		else if (yytext[i] == '\t') coluna += 4 - (coluna % 4);
		else coluna++;
	}
	ECHO; // o mesmo que printf("%s\n", yytext);
}

void yyerror(const char* msg) {
	printf("\n%*s\n%*s\n", coluna, "^", coluna, msg);
	/* printf("Erro na linha %d/%d, coluna %d em '%s'\n\n", linha, yylineno, coluna, yytext); */
	printf("Erro na linha %d, coluna %d, em '%s'\n", yylineno, coluna, yytext);

	exit(1);
}
