%error-verbose

/*
 * Declarações
 */
%{
#include "no.hpp"
#include "parser.hpp"
#include <cstdio>
#include <cstdlib>
#include <cstring>

extern int yylex(void);
extern void yyerror(const char*);
Bloco* blocoPrincipal;
%}

%union {
	std::string* string;
	int token;

	Bloco* bloco;
	Instrucao* instr;
	Expressao* expr;
	Identificador* id;
	ChamadaFuncao* chamada_funcao;
	std::vector<Expressao*>* lista_args;
	std::vector<DeclaracaoVariavel*>* lista_param;
	DeclaracaoVariavel* decl_var;
}

/*
 * Definições dos tokens
 */
/* Palavras reservadas */
%token <token> CASE DO DONE ELIF ELSE ESAC FI FOR IF IN THEN UNTIL WHILE FUNCTION DOIS_PONTOS INT DOUBLE
/* Strings */
%token <string> ID NUM_INTEIRO NUM_REAL STRING
/* Operadores aritméticos */
%token <token> AD SUB DIV MULT MOD
/* Operadores condicionais e lógicos */
%token <token> LT GT LE GE EQ NE AND OR
/* Delimitadores */
%token <token> ABRE_PAR FECHA_PAR ABRE_CH FECHA_CH ABRE_COL FECHA_COL
/* Outros */
%token <token> CIFRAO PONTO VIRGULA IGUAL EOL NIL NULO PEV

/*
 * Definições do tipo dos não-terminais
 * %type <tipo> não-terminal
 */
%type <bloco> programa seq_instr bloco_do_done bloco_funcao bloco_then
%type <id> id
%type <expr> numero expr expr_aritm expr_log expr_cond fator_aritm termo_aritm
	fator_log termo_log fator_cond
%type <lista_args> lista_args
%type <lista_param> lista_param
%type <instr> instr chamada_funcao estrutura_controle estrutura_repeticao
	estrutura_selecao def_funcao instr_while instr_if instr_until instr_for
	instr_case atrib decl_var

/* Indicação da regra onde a análise sintática começará */
%start programa

/*
 * Definições das regras sintáticas
 */
%%
programa: seq_instr { blocoPrincipal = $1; }
		;

seq_instr: lista_eol { $$ = new Bloco(); }
		 | instr lista_eol { $$ = new Bloco(); $$->lista_instrucoes.push_back($<instr>1); }
		 | seq_instr instr lista_eol { $1->lista_instrucoes.push_back($<instr>2); }
		 ;

instr: expr { $$ = new InstrucaoExpressao(*$1); }
	 | atrib
	 | decl_var
	 | estrutura_controle
	 | def_funcao 
	 | chamada_funcao 
	 | DOIS_PONTOS { $$ = new Instrucao(); }
	 ;

expr: expr_aritm
	| expr_log
	| expr_cond
	;

expr_aritm: fator_aritm
		  | expr_aritm AD fator_aritm { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		  | expr_aritm SUB fator_aritm { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		  ;

fator_aritm: termo_aritm
		   | fator_aritm MULT termo_aritm { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		   | fator_aritm DIV termo_aritm { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		   | fator_aritm MOD termo_aritm { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		   ;

termo_aritm: numero
		   | id { $<id>$ = $1; }
		   | CIFRAO id { $<id>$ = $2; }
		   | ABRE_PAR expr_aritm FECHA_PAR { $$ = $2; }
		   | SUB termo_aritm { $$ = new OperadorUnario($1, *$2); }
		   | AD termo_aritm { $$ = new OperadorUnario($1, *$2); }
		   | STRING {}
		   ;

expr_log: fator_log
		| expr_log OR fator_log { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		;

fator_log: termo_log
		 | fator_log AND termo_log { $$ = new OperacaoAritmetica(*$1, $2, *$3); }
		 ;

termo_log: expr_aritm
		 | ABRE_PAR expr_log FECHA_PAR { $$ = $2; }
		 ;

expr_cond: fator_cond
		 | expr LT fator_cond { $$ = new OperacaoCondicional(*$1, $2, *$3); }
		 | expr GT fator_cond { $$ = new OperacaoCondicional(*$1, $2, *$3); }
		 | expr LE fator_cond { $$ = new OperacaoCondicional(*$1, $2, *$3); }
		 | expr GE fator_cond { $$ = new OperacaoCondicional(*$1, $2, *$3); }
		 ;

fator_cond: termo_aritm
		  | fator_cond EQ termo_aritm { $$ = new OperacaoCondicional(*$1, $2, *$3); }
		  | fator_cond NE termo_aritm { $$ = new OperacaoCondicional(*$1, $2, *$3); }
		  ;

numero: NUM_INTEIRO { $$ = new NumeroInteiro(atol($1->c_str())); delete $1; }
	  | NUM_REAL { $$ = new NumeroReal(atof($1->c_str())); delete $1; }
	  ;

id: ID { $$ = new Identificador(*$1); }
/* id: ID { $<id>$ = new Identificador(*$1); } */
  ;

eol: EOL
   | PEV
   ;

lista_eol: %empty
		 | lista_eol eol
		 ;

def_funcao: id ABRE_PAR lista_param FECHA_PAR lista_eol bloco_funcao
		  { $$ = new DefinicaoFuncao(*$1, *$3, *$6); delete $3; }
		  | FUNCTION id ABRE_PAR lista_param FECHA_PAR lista_eol bloco_funcao
		  { $$ = new DefinicaoFuncao(*$2, *$4, *$7); delete $4; }
		  ;

lista_param: %empty { $$ = new vector<DeclaracaoVariavel*>(); }
		   | decl_var { $$ = new vector<DeclaracaoVariavel*>(); $$->push_back($<decl_var>1); }
		   | lista_param VIRGULA decl_var { $1->push_back($<decl_var>3); }
		   ; 

bloco_funcao: ABRE_CH seq_instr FECHA_CH
			{ $$ = $2; }
			/* | ABRE_CH lista_eol DOIS_PONTOS lista_eol FECHA_CH */
			/* { $$ = new Bloco(); } */
			| ABRE_CH FECHA_CH
			{ yyerror("Você precisa de pelo menos um comando aqui. Use ':' como no-op."); exit(1); }
			| ABRE_CH lista_eol FECHA_CH
			{ yyerror("Você precisa de pelo menos um comando aqui. Use ':' como no-op."); exit(1); }
			;

chamada_funcao: id lista_args { $$ = new ChamadaFuncao(*$1, *$2); delete $2; }
			  ;

lista_args: %empty { $$ = new vector<Expressao*>(); }
		  | expr { $$ = new vector<Expressao*>(); $$->push_back($1); }
		  | lista_args NULO expr { $1->push_back($3); }
		  ;

estrutura_controle: estrutura_repeticao
				  | estrutura_selecao
				  ;

estrutura_repeticao: instr_while
				   | instr_until
				   | instr_for
				   ;

instr_while: WHILE ABRE_PAR ABRE_PAR expr FECHA_PAR FECHA_PAR lista_eol bloco_do_done
		   { $$ = new InstrucaoWhile(*$4, *$8); }
		   ;

instr_until: UNTIL ABRE_PAR ABRE_PAR expr FECHA_PAR FECHA_PAR lista_eol bloco_do_done
		   { $$ = new InstrucaoUntil(*$4, *$8); }
		   ;

instr_for: FOR expr IN expr lista_eol bloco_do_done
		 { $$ = new InstrucaoFor($2, $4, *$6); }
		 | FOR ABRE_PAR ABRE_PAR instr PEV expr PEV instr FECHA_PAR FECHA_PAR lista_eol bloco_do_done
		 { $$ = new InstrucaoFor($4, $6, $8, *$12); }
		 ;

bloco_do_done: DO seq_instr DONE lista_eol { $$ = $2; }
			 ;

estrutura_selecao: instr_if
				 | instr_case
				 ;

instr_if: IF ABRE_PAR ABRE_PAR expr FECHA_PAR FECHA_PAR lista_eol bloco_then FI
		{ $$ = new InstrucaoIf(*$4, *$8); }
		| IF ABRE_PAR ABRE_PAR expr FECHA_PAR FECHA_PAR lista_eol bloco_then ELSE lista_eol seq_instr FI
		{ $$ = new InstrucaoIf(*$4, *$8, *$11); }
		| IF ABRE_PAR ABRE_PAR expr FECHA_PAR FECHA_PAR lista_eol bloco_then instr_elif FI
		{ $$ = new InstrucaoIf(*$4, *$8); }
		;

instr_elif: ELIF lista_eol
		  | ELIF expr lista_eol bloco_then ELSE lista_eol seq_instr
		  | ELIF expr lista_eol bloco_then instr_elif
		  ;

bloco_then: THEN lista_eol seq_instr { $$ = $3; }
		  ;

instr_case: CASE expr IN lista_eol seq_clausula_case lista_eol ESAC { $$ = new InstrucaoCase(*$2); }
		  ;

seq_clausula_case: clausula_case lista_eol
				 | seq_clausula_case clausula_case lista_eol
				 ;

clausula_case: expr FECHA_PAR lista_eol instr lista_eol PEV PEV
			 | MULT FECHA_PAR lista_eol instr lista_eol PEV PEV
			 ;

/* atrib: id IGUAL expr { $$ = new Atribuicao(*$1, *$3); } */
atrib: id IGUAL expr { $$ = new Atribuicao(*$<id>1, *$3); }
	 ;

decl_var: INT id { $$ = new DeclaracaoVariavel($1, *$2); }
		| DOUBLE id { $$ = new DeclaracaoVariavel($1, *$2); }
		| INT id IGUAL expr { $$ = new DeclaracaoVariavel($1, *$2, $4); }
		| DOUBLE id IGUAL expr { $$ = new DeclaracaoVariavel($1, *$2, $4); }
		| id IGUAL expr { $$ = new DeclaracaoVariavel(*$1, $3); }
		;
%%
